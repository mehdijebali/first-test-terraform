variable "ami_id" {
  default = "ami-0c6ebbd55ab05f070"
}

variable "instance_type" {
  default = "t2.micro"
}